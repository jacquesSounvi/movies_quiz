import React from 'react'

const NextStep = props => {
  return <button title={props.title} onClick={props.next}>{props.title}</button>
}

export default NextStep;