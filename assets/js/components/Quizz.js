import React, {Component} from 'react';
import axios from 'axios';
import QuizzBox from './QuizzBox';
import NextStep from './NextStep';
import Result from './Result';
import GameOver from './GameOver'
import QuizzStart from './QuizzStart'

const  API_END_POINT = "https://api.themoviedb.org/3/"
const API_END_POINT_QUIZ = "http://localhost:8000/api/quizzes/"
const  SEARCH_URL = "search/multi?"
const  API_KEY = "api_key=cefa4e18e89e4cc538ca4247a7c964be"
const IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w500"

// https://api.themoviedb.org/3/search/multi?api_key=cefa4e18e89e4cc538ca4247a7c964be&language=en-US&query=Will%20Smith

    
class Quizz extends Component {


    constructor(props) { 
        super(props); 
        this.tick = this.tick.bind(this)
        this.state = { 
          question: [],
          id:'',
          badAnswers: 0,
          score: 0,
          imgUrl: '',
          seconds: 10
          
        }; 
    } 

    componentDidMount() { 
        this.getQuestions(); 
        this.timer = setInterval(this.tick, 1000);

        
    }
    
    playAgain = () => { 
        this.getQuestions(); 
        this.setState({score: 0, responses: 0}); 
      };

      tick(){
        if (this.state.seconds > 0) {
          this.setState({seconds: this.state.seconds - 1})
        } else {
          clearInterval(this.timer);
          window.location.reload();
        }
      }


    getQuestions() {

        axios.get('http://localhost:8000/api/quizzes')
        .then(function (response) {
            // Get random questions 
            this.setState({ question: response.data['hydra:member'].sort(() => 0.5 - Math.random()).slice(0, 2)}
            )
                       
            if(this.state.question){

                if(this.state.question[0]){
                    var result = ''
                    this.setState({id:  this.state.question[0]['id']})
                    axios.get(`${API_END_POINT}${SEARCH_URL}${API_KEY}&query=${this.state.question[0]['actor']}`)
                    .then((response) => {
                        console.log("ddd",this.state.question)
                        result = response.data.results[0].profile_path;
                        this.setState({imgUrl:IMAGE_BASE_URL.concat(result)})
                        var url = IMAGE_BASE_URL + result

                        console.log(result)
                        axios.put(`${API_END_POINT_QUIZ}${this.state.question[0]['id']}`,{movie:url})
                            .then((reponse) => {
                                        console.log("up",response)
                        })

                    })
                }
                if(this.state.question[1]){
                    var result = ''
                    this.setState({id:  this.state.question[1]['id']})

                    axios.get(`${API_END_POINT}${SEARCH_URL}${API_KEY}&query=${this.state.question[1]['actor']}`)
                    .then((response) => {
                        result = response.data.results[0].profile_path;
                        var url = IMAGE_BASE_URL + result
                        this.setState({imgUrl:url})
                        axios.put(`http://localhost:8000/api/quizzes/${this.state.question[1]['id']}`,{movie:url})
                            .then((reponse) => {
                            console.log("up",response)
                        })
                    })
                }
               
            }   
            
        }.bind(this));
     };
    

    scoreAnswer(answer, correct) { 
        if (answer === correct) { 
            this.setState({disable: false})
            this.setState({isNext: true})
          this.setState({ 
            score: this.state.score + 1 
          }); 
          
        }else{
            this.setState({ 
                badAnswers: this.state.badAnswers < 2 
                  ? this.state.badAnswers + 1 
                  : 2
              }); 
            this.setState({disable: false})
            this.setState({isNext: true})
        }
    }

    render() {

        const chrono = () => {

            if(this.state.seconds == 0  && this.state.badAnswers >= 1){
                return   <GameOver score={this.state.score}/>
            }else{

                return <div style={{width: "100%", textAlign: "center"}}>
                <h1>{this.state.seconds}...</h1>
              </div>
            }
        }

            return (
                <div>     
                    {   
                        this.state.question.length > 0  && 
                        this.state.badAnswers < 2  && this.state.score < 2 &&
                        this.state.question.map(({question,answer,id,correct,movie }) => <QuizzBox   key={id} color={this.state.color} imgActor={movie} question={question.replace(/<[^>]+>/g, '').replace(/&nbsp;/g, '')} options={answer} key={id} selected={answer => this.scoreAnswer(answer,correct)} /> 
                ) 
                    }

                { 
                    this.state.score === 0 && this.state.badAnswers == 2 && <GameOver score={this.state.score} playAgain={this.playAgain}/>
                } 

                { 
                    this.state.score == 2   && <Result score={this.state.score}/>
                } 

                { 
                    this.state.score == 1 && <div className="score"> Your score is 1 / 2 correct answer</div>
                } 
               
               {
                    chrono()
               }

                    
                    


        </div>
        )
    }
}
    
export default Quizz;