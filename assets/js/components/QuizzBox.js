import React, {useState} from "react"; 
import "../../styles/app.css"; 
  
// Function to question inside our app 
const QuizzBox = ({ question, options,selected,imgActor}) => { 
  const [answer, setAnswer] = useState(options); 

  return ( 
    <div className="quizzBox">
        <div><img src={imgActor} className="imgActor"  alt="Responsive image"/></div>
        <div className="question">{question}</div> 
        {answer.map((choice, index) => ( 
          <button 
              key={index} 
              className="answerBtn"
              onClick={()=>{ 
                    setAnswer([choice]); 
                    selected(choice); 
                  }}> {choice} 
         </button> 
        ))} 
    </div> 
  ) 
}; 
  
export default QuizzBox; 