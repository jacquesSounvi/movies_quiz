import React from 'react'; 
  
const Result = ({score}) => ( 
  <div className="score-board"> 
    <div className="score"> Your score is {score} / 2 correct answer  </div> 
  </div> 
) 
  
export default Result; 