import React, {Component} from 'react';
import NextStep from './NextStep';
import Quizz from './Quizz'
 

class QuizzStart extends Component {

    constructor(props) {
        super(props)
        this.state = { 
            isEmptyState: true,
            color: props.color,
            backgroundColor: 'blue'     
        }
      }
    
      triggerNextStep = () => {
        this.setState({
          ...this.state,
          isEmptyState: false,
          isNext: true
        })
      }
    
    render() {
      return (
        <div className="">
            <div className="">
                {this.state.isEmptyState &&  <h4>Start the quizz</h4>}
                {this.state.isEmptyState && <NextStep  title='Start' next={this.triggerNextStep} /> }
                {this.state.isNext && <Quizz />}
            </div>
        </div>
      );
    }
}

export default QuizzStart;