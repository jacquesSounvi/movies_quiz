import React from 'react';
 import ReactDOM from 'react-dom';
 
 import Quizz from './js/components/Quizz';
 import NextStep from './js/components/NextStep';
 import QuizzStart from './js/components/QuizzStart';


 import './styles/app.css'

 
 
 class App extends React.Component {

    constructor(props) {
        super(props)
        this.state = { isEmptyState: true }
      }
    
      triggerNextStep = () => {
        this.setState({
          ...this.state,
          isEmptyState: false,
          isNext: true
        })
      }
   
     render() {
         return (
             <div className="container">
                <div className="title">Quizz</div>
                {this.state.isEmptyState && <QuizzStart next={this.triggerNextStep} />}

                {this.state.isNext && <Quizz />}
             </div>
         );
     }
 }
 
 ReactDOM.render(<App />, document.getElementById('root'));