Symfony 5.2 et React js

tech symfony
-----------------------------------------------------------------------
- Api plateform (symfony) qui permet de pouvoir recupérer les questions 
- Interface Admin qui permet d'ajouter les questions
-----------------------------------------------------------------------

tech React js
--------------------------------------------------------------------------------------------
- Affiche les questions provenant de l'api (combinaison entre Api plateform et themovieApi)
---------------------------------------------------------------------------------------------


Lancer le projet
-----------------------------------------------------------------------
- composer update
- php bin/console doctrine:migrations:migrate
- lancer le server de symfony dans un premier temps: php bin/console server:start 
- lancer React : yarn encore dev -- watch
-------------------------------------------------------------------------

base de données
-----------------------------------------------------------------------
- Mysql
-----------------------------------------------------------------------

Route pour la demo
------------------------------------------------------------------------
- http://localhost:8000/home => permet d'accéder aux quizz
- http://localhost:8000/api => acceder à l'api symfony
- http://localhost:8000/admin => permet d'ajouter les questions
------------------------------------------------------------------------

prerequis
-----------------------------------------------------------------------
- Pour accéder à l'api, il faut avoir rajouter les questions dans symfony
- Pour récuppérer l'image de l'acteur il le definir dans l'interface
------------------------------------------------------------------------
